import "../styles/globals.css";
import type { AppProps } from "next/app";

import { ChakraProvider } from "@chakra-ui/react";
import HeaderComponent from "../components/Header";
import FooterComponent from "../components/Footer";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider>
      <HeaderComponent />
      <Component {...pageProps} />
      <FooterComponent />
    </ChakraProvider>
  );
}
export default MyApp;
