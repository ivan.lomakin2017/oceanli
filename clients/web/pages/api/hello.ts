// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
  name: string;
};

export default (req: NextApiRequest, res: NextApiResponse<Data>) => {
  fetch(`${process.env.UPWARDLI_API_HOST}/api/v1/`)
    .then((apiRes) =>
      apiRes.status < 400 ? apiRes.text() : Promise.reject(apiRes)
    )
    .then((data) => res.status(200).json({ ok: true, content: data }))
    .catch((apiRes) =>
      res
        .status(apiRes.status)
        .json({
          ok: false,
          error: `Request failed with status ${apiRes.status}`,
        })
    );
};
