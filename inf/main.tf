terraform {
  backend "s3" {
    bucket = "upwardli-terraform-state"
    // Everything under main until we have tf modules
    key    = "main/terraform.tfstate"
    region = "us-east-2"
    dynamodb_table = "terraform-lock"
  }
}

variable "region" {
  default = "us-east-2"
}

variable "environment" {
  default = "dev"
}

provider "aws" {
  region = var.region
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.environment}-upwardli-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-2a", "us-east-2b", "us-east-2c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}

module "api_gateway_core" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = "${var.environment}-core-http"
  description   = "Proxy API Gateway for ${var.environment} Core"
  protocol_type = "HTTP"

  create_api_domain_name = false

  cors_configuration = {
    allow_headers = ["content-type", "x-amz-date", "authorization", "x-api-key", "x-amz-security-token", "x-amz-user-agent"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  integrations = {
    "ANY /" = {
      lambda_arn             = module.lambda_core.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }

    "$default" = {
      lambda_arn = module.lambda_core.lambda_function_arn
    }
  }
}

module "lambda_core" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "core-${var.environment}"
  description   = "backend/core Lambda"

  create_package = false
  package_type   = "Image"

  // placeholder 
  image_uri = "490799294904.dkr.ecr.us-east-2.amazonaws.com/core:70d79de361216f1935a69bd7bffa39c05cefb8cb"

  vpc_subnet_ids         = module.vpc.public_subnets
  vpc_security_group_ids = [module.vpc.default_security_group_id]
  attach_network_policy  = true
  publish = true

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${module.api_gateway_core.apigatewayv2_api_execution_arn}/*/*/*"
    }
  }
}
